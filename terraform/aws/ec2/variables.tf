variable region {

  description = "Name of AWS region:"
  default = "eu-west-1"

}

variable instance_type {

  description = "EC2 instance type:"
  default = "t2.micro"

}

variable public_key {

  description = "Public key for ssh connections:"

}
